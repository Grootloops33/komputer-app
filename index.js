//finding and assigning a bunch of elements
const balText = document.getElementById("balance");
const loanText = document.getElementById("loan");
const payText = document.getElementById("salary");
const dropdown = document.getElementById("laptopDropdown");
const specText = document.getElementById("specList");
const adTitle = document.getElementById("adTitle");
const adDesc = document.getElementById("adDesc");
const adImg = document.getElementById("adImage");
const adPrice = document.getElementById("adPrice");

//initiating some global variables
let bal = 0;
let workBal = 0;
let outLoan = 0;
let hasLoan = false;
let index = 0;

//find buttons and assign their clicky click
let btn_loanButton = document.getElementById("loanButton");
    btn_loanButton.addEventListener("click", GetLoan);
    btn_loanButton = document.querySelector("button");

let btn_payLoanButton = document.getElementById("payLoanButton");
    btn_payLoanButton.addEventListener("click", PayLoan)
    btn_payLoanButton = document.querySelector("button");

let btn_depositSalary = document.getElementById("depositSalary");
    btn_depositSalary.addEventListener("click", DepositSalary);
    btn_depositSalary = document.querySelector("button");

let btn_workButton = document.getElementById("workButton");
    btn_workButton.addEventListener("click", DoWork);
    btn_loanButton = document.querySelector("button");

let btn_dropbtn = document.getElementById("dropbtn");
    btn_dropbtn.addEventListener("click", ShowDropdown);
    btn_dropbtn = document.querySelector("button");

let btn_buybtn = document.getElementById("buybtn");
    btn_buybtn.addEventListener("click", BuyNow);
    btn_buybtn = document.querySelector("button");


//Initial UpdateBalance() call to set the all the money correctly.
UpdateBalance();

// Asynchronous function to fetch the API from the server.
async function getAPI()
{
    try
    {
        let o = await fetch('https://noroff-komputer-store-api.herokuapp.com/computers')
        return o.json();    //returning json object
    }
    catch(error)
    {
        console.log("error" + error);
    }
    finally
    {
        console.log("done");
    }
}

//console.log(await getAPI());

//assigning the API to this variable
let megaObj = await getAPI();
//initating the dropdown Menu using the newly acquired API
LoadDropdown();
UpdateAd();
LoadFeatures();

function LoadDropdown()
{
    for(let i = 0; i < megaObj.length; i++) // looping through the object results array
    {
        const opt = megaObj[i]; // for simplicity sake, setting opt to be equal to the i'th instance of megaObj
        let el = document.createElement("opt"); //creating option element
        el.textContent = `${megaObj[i]['title']}`; //setting the text content of the dropdown to be the title of the item
        el.addEventListener("click", () => {
            index = i; //setting global variable index equal to I;
            LoadFeatures();
            UpdateAd();
        });
        // adding and calling the functions required for clicking to work as intended.
        el.querySelector("button"); //setting button functionality
        let lb = document.createElement("br"); //Linebreak to make space.
        
        el.value = opt;

        dropdown.appendChild(el);
        dropdown.appendChild(lb);

        //adding the element and the linebreak as children to the dropdown.
    }
}

function LoadFeatures()
{
    //clearing out all the specs already in the features list
    while(specText.firstChild)
    {
        specText.removeChild(specText.firstChild);
    }

    //looping through the specs of megaObj[index] to find all the specs of the selected(index) item;
    for(let i = 0; i < megaObj[index]['specs'].length; i++)
    {
        console.log(i);
        console.log(megaObj[index]['specs'][i]);

        let opt = megaObj[index]['specs'][i] // variable called opt, because this was a copy paste of the LoadDropdown's loop. and I didn't find it necessary to change the variable name
        let el = document.createElement("p"); //creating paragraph element
        el.textContent = `${megaObj[index]['specs'][i]}`; //setting the content to have the i'th element of the currently selected(index) specs.
        el.className=`c${index}`; //setting css class
        el.id=`c${index}`; //setting id
        //NOTE: I know I can use either, or, but I've decided to use both due to unknown reasons. The universe works in mysterious ways.
        el.style.textAlign = 'left'; //Making sure the element's text stays on the left.
        el.value = opt; //setting it to opt aka megaObj[index]['specs'][i]

        specText.appendChild(el); //adding each element to the specs section.
    }
}

function GetLoan()
{
    //boolean check to see if the user currently has a loan active or not.
    if(hasLoan)
    {
        window.alert(`Warning; you can only have one loan active, Please pay your loan first.`)
        return; //if we have a loan active, return; ending the function.
    }

    let inc = ~~parseInt(prompt("How much would you like to loan?")); //parsing the prompt window, and then using a math.floor (~~) substitute to prevent NaN.

    if(inc === 0)
    {
        window.alert(`Warning; You didn't enter a number we've canceled the loan`)
        return; //if the user didn't enter a number we're canceling the loan.
    }

    if(inc > (bal*2))
    {
        //checking if the requested loan amount is more than double your current balance, if it is, we'll loan the max amount available for you.
        inc = bal*2; 
        window.alert(`Warning; you can not loan more than double you are worth. Loaned max amount of ${inc}.`);
    }

    hasLoan = true; //User now has a loan, setting boolean value to true.
    outLoan += inc; //setting the current amount of loan outgoing.
    bal += inc; // adding balance, happy spending!

    UpdateBalance(); //updating balance visually.
}

function PayLoan()
{
    if(outLoan === 0) //can't pay back a loan if the outgoing loan is nonexistant.
    {
        window.alert(`Warning; you do not have a loan open.`) 
        return; //return; ending the function if there's no loan.
    }
    bal -= outLoan; //paying off the loan, lucky for you there's no interest.
    outLoan = 0; //setting the outgoing loan to 0
    hasLoan = false; //the user no longer has a loan, so setting the boolean to falce

    UpdateBalance(); //updating balance visually.
}

function DoWork()
{
    if(outLoan > 0)
    {
        outLoan -= 10;
        workBal += 90;
    }
    else
    {
        workBal += 100;
    }

    UpdateSalary(); //updating salary visually.
    UpdateBalance(); //updating balance visually.
}

function DepositSalary()
{
    bal += workBal;
    workBal = 0;

    UpdateSalary(); //updating salary visually.
    UpdateBalance(); //updating balance visually.
}

function UpdateBalance()
{
    //setting text
    balText.innerText = `${bal} Kr.`;
    loanText.innerText = `${outLoan} Kr.`;
}
function UpdateSalary()
{
    //setting text
    payText.innerText = `${workBal} Kr.`;
}
function ShowDropdown() 
{
    // hiding / showing the dropdown
    document.getElementById("laptopDropdown").classList.toggle("show");
}

function UpdateAd()
{
    adTitle.textContent = megaObj[index]['title']; //setting the titleContent to be the megaObj's title.
    adDesc.textContent = megaObj[index]['description']; //setting the titleContent to be the megaObj's description.
    adPrice.textContent = megaObj[index]['price'] + " Kr"; //setting the titleContent to be the megaObj's price and adding " Kr" to that.
    if(index != 4)
    {
        adImg.src = "https://noroff-komputer-store-api.herokuapp.com/" + megaObj[index]['image']
    }
    else //very dirty fix because 'broken' API
    {
        adImg.src = "https://noroff-komputer-store-api.herokuapp.com/assets/images/5.png";
    }
}

function BuyNow()
{
    // console.log(index);
    // console.log(megaObj[index]['price']);
    if(workBal >= megaObj[index]['price'])
    {
        workBal -= megaObj[index]['price']; //taking the money from you! haha!
    }
    else
    {
        window.alert(`Warning; You currently do not enough money (${megaObj[index]['price']})to pay for this item You have: ${bal}`) //not taking the money from you :(
    }

    UpdateBalance(); //updating balance visually.
}